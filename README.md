# List of books app

![App preview](./preview.png)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Description

Project is as much RWD friendly as I could do in such short period of time.

I used json file which I found on the web, the most challenging thing was to load file into the app, but I've used fetch function for that. To make the UX more friendly I used React loader on the screen.

I didn't want to create a custom solution so based on the stars count I used
[react-virtualized](https://github.com/bvaughn/react-virtualized)

Sorting that huge json was not an easy thing for browser so I created webworker to move those hard calculations out of the main thread to not freeze the browser UI. That was my first experience with this tool so it was a great lesson TIL.

I care about tests so feel free to run it. I couldn't develop all of the functionalities but I am sure that it is enough to show my JS skills.
