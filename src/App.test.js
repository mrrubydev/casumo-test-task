import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("render React loading bar", () => {
  const { getByAltText } = render(<App />);
  const reactLogo = getByAltText(/logo/i);
  expect(reactLogo).toBeInTheDocument();
});
