export default () => ({
  sortAuthors: jest.fn(),
  addEventListener: jest.fn()
});
