import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Actions from "./Actions";

describe("Component: Actions", () => {
  const sortByAuthor = jest.fn();
  const turnOff = jest.fn();
  const defaultProps = {
    sortByAuthor,
    turnOff,
    listEmpty: false
  };

  it("should click 'Sort by author' function on proper text", () => {
    const { getByText } = render(<Actions {...defaultProps} />);
    fireEvent.click(getByText("Sort by author"));
    expect(sortByAuthor).toHaveBeenCalled();
  });
  it("should click 'Turn off filters' function on proper text", () => {
    const { getByText } = render(<Actions {...defaultProps} />);
    fireEvent.click(getByText("Turn off filters"));
    expect(sortByAuthor).toHaveBeenCalled();
  });
});
