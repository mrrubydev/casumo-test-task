import React from "react";
import ActionButton from "./ActionButton/ActionButton";
import "./Actions.css";

function Actions({ sortByAuthor, turnOff, listEmpty }) {
  return (
    <div className="control">
      <ActionButton
        isEmpty={listEmpty}
        onClick={sortByAuthor}
        text={"Sort by author"}
      />
      <ActionButton
        isEmpty={listEmpty}
        onClick={turnOff}
        text={"Turn off filters"}
      />
    </div>
  );
}

export default Actions;
