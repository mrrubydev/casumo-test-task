import React from "react";

import "./ActionButton.css";

function ActionButton({ isEmpty, onClick, text }) {
  return (
    <button
      disabled={isEmpty}
      className={`action ${isEmpty && "animation"}`}
      onClick={onClick}
    >
      <span className="text">{text}</span>
    </button>
  );
}

export default ActionButton;
