import React, { useEffect, useState, useRef } from "react";
import { Column, Table } from "react-virtualized";
import ColumnButton from "./ColumnButton/ColumnButton";
import Actions from "./Actions/Actions";
import { resolvePath, isArray } from "../../helpers";
import { tabMap } from "../../constants";
import logo from "../../logo.svg";
import "./BooksTable.css";
import worker from "workerize-loader!../../sort.worker"; // eslint-disable-line import/no-webpack-loader-syntax

const BOOKS_API_ENDPOINT = "./data/myBooks.json";

const workerInstance = worker();

function BooksTable() {
  const listRef = useRef(null);
  const [list, setList] = useState([]);
  const [currentColumn, setCurrentColumn] = useState("name");
  const listEmpty = list.length === 0;

  const loadBooks = async () => {
    const response = await fetch(BOOKS_API_ENDPOINT);
    const data = await response.json();
    setList(data);
  };

  const sortByAuthor = () => {
    workerInstance.sortAuthors(list);
    setList([]);
  };

  const turnOff = () => {
    setList([]);
    loadBooks();
  };

  useEffect(() => {
    loadBooks();
    workerInstance.addEventListener("message", event => {
      const sortedList = event.data;
      if (isArray(sortedList)) {
        setList(sortedList);
        listRef.current.forceUpdateGrid();
      }
    });
  }, []);

  return (
    <>
      <div className="outer container">
        <div className="container">
          <p className="title">{`List of books`}</p>
          <div className="tab">
            {Object.keys(tabMap).map(key => (
              <ColumnButton
                key={key}
                name={key}
                currentColumn={currentColumn}
                onClick={() => setCurrentColumn(tabMap[key])}
              />
            ))}
          </div>
          <Table
            ref={listRef}
            width={300}
            height={300}
            headerHeight={20}
            rowHeight={30}
            rowCount={list.length}
            rowGetter={({ index }) => list[index]}
            noRowsRenderer={() => (
              <img src={logo} className="App-logo" alt="logo" />
            )}
          >
            <Column
              dataKey={currentColumn}
              cellDataGetter={({ rowData }) =>
                resolvePath(rowData, currentColumn)
              }
              width={100}
            />
          </Table>
        </div>
      </div>
      <Actions {...{ sortByAuthor, turnOff, listEmpty }} />
    </>
  );
}

export default BooksTable;
