import React from "react";
import { render, act } from "@testing-library/react";
import data from "../../../public/data/mybooks.json";
import mockedData from "../../test/mock/mybooks.json";
import BooksTable from "./BooksTable";

describe("Component: BooksTable", () => {
  it("gets 1000000 books from file", async () => {
    jest
      .spyOn(global, "fetch")
      .mockImplementation(() => ({ json: () => data }));
    await act(async () => {
      render(<BooksTable />);
    });
    expect(data.length).toBe(1000000);
  });

  it("renders sample book from mock", async () => {
    jest
      .spyOn(global, "fetch")
      .mockImplementation(() => ({ json: () => mockedData }));

    let component;
    await act(async () => {
      component = render(<BooksTable />);
    });
    const { getByText } = component;
    const element = getByText(mockedData[0].name);
    expect(element.textContent).toBeTruthy();
  });
});
