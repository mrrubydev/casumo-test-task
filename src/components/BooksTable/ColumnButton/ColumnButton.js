import React from "react";
import { tabMap } from "../../../constants";
import "./ColumnButton.css";

function ColumnButton({ onClick, name, currentColumn }) {
  return (
    <button
      className={`tablinks ${tabMap[name] === currentColumn && "active"}`}
      onClick={onClick}
    >
      {name}
    </button>
  );
}

export default ColumnButton;
