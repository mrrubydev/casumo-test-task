import React from "react";
import { render, fireEvent } from "@testing-library/react";
import ColumnButton from "./ColumnButton";

describe("Component: ColumnButton", () => {
  it("renders clickable button", () => {
    const onClick = jest.fn();
    const name = "test";
    const { getByText } = render(<ColumnButton {...{ name, onClick }} />);

    fireEvent.click(getByText(name));
    expect(onClick).toHaveBeenCalled();
  });
});
