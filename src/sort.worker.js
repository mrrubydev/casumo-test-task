import { sortBy } from "./helpers";
import { tabMap } from "./constants";

export const sortAuthors = array => {
  postMessage(sortBy(array, tabMap.Author));
};
