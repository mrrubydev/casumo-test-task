import React from "react";
import BooksTable from "./components/BooksTable/BooksTable";
import "./App.css";

function App() {
  return (
    <div className="App">
      <BooksTable />
    </div>
  );
}

export default App;
