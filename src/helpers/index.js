export const resolvePath = (object, path, defaultValue) =>
  path.split(".").reduce((o, p) => (o ? o[p] : defaultValue), object);

export const isArray = obj => !!obj && obj.constructor === Array;

export const sortBy = (arr, by) =>
  arr.sort((a, b) => resolvePath(a, by).localeCompare(resolvePath(b, by)));
