import { resolvePath, isArray, sortBy } from "./index.js";
import { tabMap } from "../constants";
import mockedData from "../test/mock/mybooks.json";

describe("Helpers:", () => {
  it("should check if resolvePath works", () => {
    const testObject = mockedData[0];

    const testArr = ["name", "name.author", "gender", "author.name"];
    const results = ["Gurov, Murder", undefined, undefined, "Marie Oliver"];

    testArr.map((element, index) => {
      expect(resolvePath(testObject, element)).toBe(results[index]);
    });
  });

  it("should check if it isArray", () => {
    const testArr = [[], { test: 0 }, {}, "test", 1, false, [1, 2]];

    const trulyIndex = [0, 6];

    testArr.map((element, index) => {
      expect(isArray(element)).toBe(trulyIndex.includes(index));
    });
  });

  it("should sortBy: Author", () => {
    const result = sortBy(mockedData, tabMap.Author);

    expect(result[0].author.name).toBe("Adele Swanson");
    expect(result[1].author.name).toBe("Charlie Santos");
    expect(result[2].author.name).toBe("Devin Webster");
  });
});
